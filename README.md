# How the browser renders a web page?
In this blog, we learn how the browser renders a html, css files to show contents of web application. The main concept here is **tree like strcuture** or **Rendertree**

First we need to know the words like **DOM** and **CSSOM** to understand how the browser renders a webpage.

## Fullforms of DOM and CSSOM:
    
> **DOM**     - Document Object Model\
> **CSSOM**   - CSS Object Model

## Some of the common problems a website may encounter:
* Slow loading of the resources.
* Waiting for unnecessary files to download.
* flash of unstyled content(FOUC)

When a browser sends a request to a server to fetch an HTML document, the server returns an HTML page in **binary stream format** which is basically a text file with the response header Content-type set to the value text/html; charset=UTF-8.

Here text/html is a [MIME Type](https://en.wikipedia.org/wiki/Media_type) which tells the browser that it is an HTML document and charset=UTF-8 tells the browser that it is encoded in [UTF-8](https://en.wikipedia.org/wiki/UTF-8) character encoding. Using this information, the browser can convert the binary format into a readable text file. This has shown below in the screenshot

![image](./browser-console.png)

To know how the browser will render the webpage. First we need to understand the what is **DOM**, **CSSOM**, and **Render Tree**?

## Understanding the Critical Rendering Path:
    1) Constructing the DOM Tree
    2) Constructing the CSSOM Tree
    3) Running JavaScript
    4) Creating the Render Tree
    5) Generating the Layout
    6) Painting

![Critical Rendering Path](./critical-rendering-path.png)

## 1. Constructing the DOM Tree
The browser looks for the html page. And then browser parse the html page into **tree-like structure**. The DOM ([Document Object Model](https://dom.spec.whatwg.org/)) Tree is an Object representation of the fully parsed HTML page. Starting with the root element **\<html>** nodes are created for each element/text on the page. Elements nested within other elements are represented as child nodes and each node contains the full attributes for that element. For example, an **\<a>** element will have the **href** attribute associated with it’s node. 

Take, for example, this sample document -

```
<html>
<head>
  <title>Understanding the Critical Rendering Path</title>
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <header>
      <h1>Understanding the Critical Rendering Path</h1>
  </header>
  <main>
      <h2>Introduction</h2>
      <p>Lorem ipsum dolor sit amet</p>
  </main>
  <footer>
      <small>Copyright 2017</small>
  </footer>
</body>
</html>
```

This will create the following DOM Tree -

![DOM](./DOM.png)

A good thing about HTML is that it can be executed in parts. The full document doesn't have to be loaded for content to start appearing on the page. However, other resources, CSS and JavaScript, can block the render of the page.

## 2. Constructing the CSSOM Tree
The CSSOM ([CSS Object Model](https://www.w3.org/TR/cssom-1/)) is an Object representation of the styles associated with the DOM. It is represented in a similar way to the DOM, but with the associated styles for each node, whether they explicitly declared or implicitly inherited, included.

In the **style.css** file from the document mentioned above, we have the folowing styles

```
body { font-size: 18px; }

header { color: plum; }
h1 { font-size: 28px; }

main { color: firebrick; }
h2 { font-size: 20px; }

footer { display: none; }
```
This will create the following CSSOM Tree -

![CSSOM](./CSSOM.png)

CSS is considered a **"render blocking resource"**. This means that the Render Tree (see below) cannot be constructed without first fully parsing the resource.

CSS can also be **"script blocking"**. This is because JavaScript files must wait until the CSSOM has been constructed before it can run

## 3. Running JavaScript

JavaScript is considered a **"parser blocking resource"**. This means that the parsing of the HTML document itself is blocked by JavaScript.

When the parser reaches a **\<script>** tag, whether that be internal or external, it stops to fetch (if it is external) and run it. This why, if we have a JavaScript file that references elements within the document, it must be placed after the appearance of that document.

To avoid JavaScript being parser blocking, it can be loaded asynchronously be applying the **async** attribute.

```
<script async src="script.js">
```

## 4. Creating the Render Tree

The Render Tree is a combination of both the DOM and CSSOM. It is a Tree that represents what will be eventually rendered on the page. This means that it only captures the visible content and will not include, for example, elements that have been hidden with CSS using **display: none**.

Using the example DOM and CSSOM above, the following Render Tree will be created -

![Render Tree](./Render-Tree.png)

## 5. Generating the Layout
The Layout is what determines what the size of the viewport is, which provides context for CSS styles that are dependent on it, e.g. percentage or viewport units. The viewport size is determined by the meta viewport tag provided in the document head or, if no tag is provided, the default viewport width of 980px is used.

For example, the most common meta viewport value is to set the viewport size to correspond to the device width -

```
<meta name="viewport" content="width=device-width,initial-scale=1">
```

The Size of the view port based on the user meta data in html document.

## 6. Paiting
Finally, in the Painting step, the visible content of the page can be converted to pixels to be displayed on the screen.

How much time the paint step takes depends on the size of the DOM, as well as what styles are applied. Some styles require more work to execute than others. For example, a complicated gradient background-image will require more time than a simple solid background colour.
